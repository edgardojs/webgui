# Instabot WebGui (In-Development)
Instabot Webgui is a bot which can watch stories by username and like all comments, it also has multibot where it runs more than one bot on different threads. More functions will come over the next days. 

It will end up being implemented to InstabotAI
https://github.com/instabotai/instabotai/

![image](https://i.imgur.com/yv9eAyv.png)

## Features
* Watch Infinity Stories by user
* Like Users Following Images
* Like Users Followers Images
* Like Hashtag Images
* Like all image comments
* Multibot
* GUI

### To Install:
git clone https://github.com/instabotai/webgui.git

pip install -r requirements.txt

### Run: 
python run.py -u yourusername -p yourpassword

Works on all browsers without extensions!
